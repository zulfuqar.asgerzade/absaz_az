import React, { Component } from 'react';
import './scss/home.scss';
import 'font-awesome/css/font-awesome.min.css';

export default class Home extends Component {
    render() {
        return (
            <div>
                {/* HEADER */}
                <section className="header">
                    <div className="welcomeContainer">
                        <nav className="welcomeContainer_navbar">
                            <img src={require("./img/logo.png")} alt="logo"/>
                            <ul className="welcomeContainer_navbarBtnContainer">
                                <li>Giriş</li>
                                <li>Qeydiyyatdan keç</li>
                                <li>Müəllim tap</li>
                            </ul>
                        </nav>
                        <div className="welcomeContainer_centerContainer">
                            <h1 className="welcomeContainer_slogan">Ən yaxşı müəllimləri bizimlə kəşf et!</h1>
                            <button className="startBtn">Başla!</button>
                        </div>
                    </div>
                    <div className='header_second'>
                        <img src={require("./img/backPhoto.png")} alt="wallpaper"/>
                    </div>
                </section>
                {/* BENEFITS */}
                <section className="benefits">
                    <div className="benefitsContainer">
                        <div className="benefitsContent">
                            <h2 className="benefitsContent_header">Necə işləyir?</h2>
                            <div className="benefitCards">
                                <div className="benefitCards_card">
                                    <div className="benefitCards_imageContainer" style={{backgroundImage: `url(${require('./img/benefit1.png')})`}}></div>
                                    <div className="benefitCards_lines"></div>
                                    <p className="benefitCards_txt">Saytımızla tanış olduqdan sonra qeydiyyatdan keçin.</p>                                    
                                </div>
                                <div className="benefitCards_card">
                                    <div className="benefitCards_imageContainer" style={{backgroundImage: `url(${require('./img/benefit2.png')})`}}></div>
                                    <div className="benefitCards_lines"></div>
                                    <p className="benefitCards_txt fixingText">Bölgə və region üzrə sizə ən yaxın ola müəllimləri əlavə vaxt xərcləmədən tapın.</p>
                                </div>
                                <div className="benefitCards_card">
                                    <div className="benefitCards_imageContainer" style={{backgroundImage: `url(${require('./img/benefit3.png')})`}}></div>
                                    <div className="benefitCards_lines"></div>
                                    <p className="benefitCards_txt">Müəllimlərin köməyi ilə uğur qazanın.</p>
                                </div>
                            </div>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="100%" viewBox="0 0 1922 1100">
                            <path className="benefitsContainer_wall" d="M-1.008,246.019s157.949,97.917,797.44-124.076C1435.56-99.924,1782,37.859,1922.62,93.931c0.83,2.567,0,1133.069,0,1133.069H-0.405Z"/>
                        </svg>
                    </div>
                </section>
                {/* TEACHER */}
                <section className="teacherAds">
                    <div className="teacherAds_imgContainer">
                        <img src={require('./img/einstein.png')} alt="einsten"/>
                        {/* <div className="half"></div> */}
                    </div>
                    <div className="teacherAds_slogan">
                        <h3 className="teacherAds_header">Müəllimsən?</h3>
                        <p className="teacherAds_txt">
                            Öz müəllimlik kariyeranızda ən zirvə nöqtəyə bizimlə çatın. Bölgə üzrə abituryentlərin sizi tapmasına köməklik edirik!
                        </p>
                        <button className="startBtn">Başla!</button>
                    </div>
                </section>
                {/* STUDENT */}
                <section className="studentAds">
                    <img src={require('./img/girl.png')} alt="girl" className="studentAds_girlImg"/>
                    <div className="studentAds_contentContainer">
                        <p className="studentAds_header">Tələbəsən?</p>
                        <p className="studentAds_txt">Müəllimini tap</p>
                        <button className="studentAds_searchBtn"></button>
                    </div>
                    <img src={require('./img/whiteDots.png')} alt="whiteDots" className="studentAds_whiteDots"/>
                </section>
                {/* FOOTER */}
                <section className="footer">
                    <ul className="footer_navigation">
                        <li><a href="https://google.com/">Qeydiyyatdan keç</a></li>
                        <li><a href="https://google.com/">Daxil ol</a></li>
                        <li><a href="https://google.com/">Müəllim tap</a></li>
                    </ul>
                    <ul className="footer_contact">
                        <li>
                            {/* <i className="far fa-envelope"></i> */}
                            <span>info@abzas.az</span>
                        </li>
                        <li>
                            {/* <i className="fas fa-mobile-alt"></i> */}
                            <span>(+994 70) 365 12 30</span>
                        </li>
                    </ul>
                    <div className="footer_newsContainer">
                        <p className="footer_getNewsTxt">Yeniliklərdən xəbərdar olun</p>
                        <input type="mail" className="footer_input" placeholder="nümunə@mail.com"/>
                        <button className="footer_sendBtn">Göndər</button>
                    </div>
                </section>
            </div>
        )
    }
}