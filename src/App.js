import React from 'react';
import './App.css';
import Home from './component/home/index'

function App() {
  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
